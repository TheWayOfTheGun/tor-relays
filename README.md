# Tor Relays

Deployment for Tor relays on FreeBSD.

These files automate provisioning and configuration of my Tor relays on
DigitalOcean. Terraform provisions volumes, VMs, and floating IPs, then
invokes a shell script on the relay host to comfigure it.

The config script is meant to be idempotent—you can invoke it multiple times
without ill effect. If you find an exception, that's a bug.

I'm running the deployments from MacOS. Other than the use of `say` to
announce server creation & destruction, everything should work on other Unix-
like OSs as well.

## DigitalOcean volume

On each relay, Tor's data lives on a persistent volume which will survive
recreating the underlying host. That way when we rebuild the VM our relay will
keep the same identity (and thus fingerprint), logs, and other data.

The persistent volume is mounted at `/tor`.

Our `.torrc` relocates the following onto our persistent volume:

    /var/log/tor -> /tor/log
    /var/db/tor -> /tor/db
    /usr/local/etc/tor -> /tor/etc

## What's here

- **TODO.md** — Future work
- **host-config** — These files are copied into `/root/host-config` to
  configure the host once it is running. README.md in that directory has
  more info.
- **tor.tf** — Terraform config
- **provider.tf** — Supporting config for tor.tf

## Requirements

- Terraform
- make
- A DigitalOcean account

## Make it go

		git clone https://gitlab.com/TheWayOfTheGun/tor-relays.git
		cd tor-relays

    cp digitalocean.tfvars.example digitalocean.tfvars
    cp backend.tf.example backend.tf

Fill in contents of `digitalocean.tfvars` and `backend.tf` then:

		make

## Relay family

If you run more than one relay, each should use the `MyFamily` configuration
option to delare its siblings. (See "My Family" in the [
Tor manual](https://www.torproject.org/docs/tor-manual.html.en) for details.)

`MyFamily` uses relay fingerprints. When we create a new relay, we don't know
its fingerprint until the relay starts for the first time. This means we need
an extra step any time you create a brand new relay:

    make update-fingerprints

That will fetch the fingerprint from each relay then update the relay congigs
so they know about each other.

Note: If you lose the contents of `/tor/db` on a relay, that relay will
generate a new fingerprint next time it starts up.

## License

This project is available under the MIT license. See [LICENSE.md](LICENSE.md)
for more information.

#!/usr/bin/env bash

set -eu

my_username=${my_username:-'mkb'}
conf_dir=${conf_dir:-'/root/host-config'}

RCol='\033[0m'
BCya='\033[1;36m'
BGre='\033[1;32m'
BRed='\033[1;31m'
BWhi='\033[1;37m'
BYel='\033[1;33m'
Cya='\033[0;36m'
Gre='\033[0;32m'
Yel='\033[0;33m'

function announce-started {
  echo -e "\n\n${BCya}---"
  echo -e "${BCya}${0} started."
  echo -e "${BCya}---${RCol}"
  echo
  echo
}

function announce-task {
  echo -e "\n\n${BWhi}---"
  echo -e "${BWhi}${1}"
  echo -e "${BWhi}---${RCol}"
}

function announce-success {
  echo -e "${BGre}${@}${RCol}"
}

function run-cmd {
  echo -e "\n${Cya}${@}${RCol}\n"
  eval $@
}

function warn {
  echo -e "${BYel}${@}${RCol}"
}

function fail {
  echo -e "${BRed}FAILURE: ${@}${RCol}"
  echo -e "${BRed}Exiting...${RCol}"
  exit -1
}

function update-everything {
  announce-task "Looking for updates."

  if grep -q latest /etc/pkg/FreeBSD.conf; then
    announce-success "Pkg already set for latest"
  else
    announce-task "Updating Pkg system to use latest instead of quarterly."
    run-cmd sed -I '.bak' "s/quarterly/latest/g" /etc/pkg/FreeBSD.conf
  fi

  if grep -q '\bhttps\b' /etc/pkg/FreeBSD.conf; then
    announce-success "Pkg already set for HTTPS"
  else
    announce-task "Updating Pkg system to use HTTPS."
    run-cmd sed -I '.bak' "s/http/https/g" /etc/pkg/FreeBSD.conf
  fi

  run-cmd env PAGER=cat freebsd-update --not-running-from-cron fetch install
  run-cmd pkg upgrade -y
}

function install-package {
  local package=$1
  local executable=${2:-''}
  if [[ -z $executable ]]; then
    executable=$(basename ${package})
  fi

  if hash "${executable}" 2>/dev/null; then
    announce-success "${package} already installed."
  else
    announce-task "Installing ${package}."
    run-cmd pkg install -y ${package}
  fi
}

function install-packages {
  announce-task "Installing packages..."

  for package in $@; do
    install-package ${package}
  done
}

function create-my-user {
  announce-task "Creating user ${my_username}"
  if getent passwd ${my_username} > /dev/null 2>&1; then
    announce-success "User ${my_username} already exists"
  else
    run-cmd pw useradd -n ${my_username} -m -d /home/${my_username} -c "${my_username}" -s /usr/local/bin/zsh
    run-cmd mkdir -p ~mkb/.ssh
    run-cmd chmod 700 ~mkb/.ssh/
    run-cmd cp ~/.ssh/authorized_keys ~mkb/.ssh/
    run-cmd chown -R mkb ~mkb

    if [[ -f ${conf_dir}/50-mkb-sudoer ]]; then
      run-cmd mv ${conf_dir}/50-mkb-sudoer /usr/local/etc/sudoers.d/50-mkb-sudoer
    fi
  fi

  if [[ -f ${conf_dir}/zshrc.zsh ]]; then
    announce-task 'Configuring zsh.'
    run-cmd mv ${conf_dir}/zshrc.zsh ~mkb/.zshrc
    run-cmd chown -R mkb:mkb ~mkb/.zshrc
  fi

  if [[ -f ${conf_dir}/nyxrc ]]; then
    announce-task 'Configuring Nyx.'
    run-cmd mkdir -p ~mkb/.nyx
    run-cmd mv ${conf_dir}/nyxrc ~mkb/.nyx/config
    run-cmd chown -R mkb:mkb ~mkb/.nyx
  fi
}

function prep-and-mount-volume {
  if [[ -n $(gpart show da0) ]]; then
    announce-success "Volume already partitioned and formatted."
  else
    announce-task "Partitioning and formatting volume."

    run-cmd gpart create -s gpt da0
    run-cmd gpart show da0
    run-cmd gpart add -t freebsd da0
    run-cmd newfs /dev/da0s1
  fi

  mkdir -p /tor
  if grep -q /tor /etc/fstab ; then
    announce-success 'Fstab entry exists.'
  else
    # echo -e "\n\n${Cya}echo '/dev/da0s1 /tor ufs rw,noatime 0 0' >> /etc/fstab${RCol}\n"
    announce-task 'Adding /tor to fstab'
    run-cmd "echo '/dev/da0s1 /tor ufs rw,noatime 0 0' >> /etc/fstab"
  fi

  if df /tor | grep -q /tor; then
    announce-success 'Volume already mounted.'
  else
    run-cmd fsck -y da0s1 || run-cmd fsck -y da0s1 || run-cmd fsck -y da0s1
    run-cmd mount /tor || fail "Could not mount /tor"
  fi
}

function install-mo {
  if [[ -x /usr/local/bin/mo ]]; then
    announce-task "Mo already installed"
  else
    announce-task "Installing mo"

    run-cmd curl -sSL https://git.io/get-mo -o /usr/local/bin/mo
    run-cmd chmod +x /usr/local/bin/mo
  fi
}

function prep-tor-directories {
  run-cmd mkdir -p /tor/log
  run-cmd mkdir -p /tor/etc
  run-cmd mkdir -p /tor/db && chmod 700 /tor/db

  build-torrc

  run-cmd chown -R _tor:_tor /tor

  [ -d /var/log/tor ] && rmdir /var/log/tor
  [ -d /var/db/tor ] && rmdir /var/db/tor
  run-cmd rm -rf /usr/local/etc/tor
}

function build-torrc {
  chmod +x ${conf_dir}/fetch-user-data.sh
  NICKNAME=$(${conf_dir}/fetch-user-data.sh relay_nickname)
  HASHED_CONTROL_PASSWORD=$(${conf_dir}/fetch-user-data.sh hashed_control_password)

  chmod +x ${conf_dir}/construct-myfamily.sh
  MYFAMILY=$(${conf_dir}/construct-myfamily.sh)

  run-cmd "NICKNAME=$NICKNAME
    MYFAMILY=$MYFAMILY
    HASHED_CONTROL_PASSWORD=$HASHED_CONTROL_PASSWORD
    mo ${conf_dir}/torrc.mo > /tor/etc/torrc"
}

function configure-and-start-tor {
  announce-task "Configuring and starting Tor."

  prep-tor-directories

  run-cmd sysrc tor_enable=YES
  run-cmd sysrc tor_conf=/tor/etc/torrc
  run-cmd sysrc tor_datadir=/tor/db

  run-cmd 'service tor start || echo'
}

function install-and-configure-pf {
  announce-task "Setting up pf"

  install-package pftop

  if [[ -f ${conf_dir}/pf.conf ]]; then
    run-cmd mv ${conf_dir}/pf.conf /etc
  fi

  run-cmd sysrc pf_enable="YES"
  run-cmd sysrc pf_flags=""
  run-cmd sysrc pf_rules="/etc/pf.conf"
  run-cmd sysrc pflog_enable="YES"
  run-cmd sysrc pflog_logfile="/var/log/pflog"

  run-cmd 'service pf start || echo'
  run-cmd 'service pflog start || echo'
}

function install-and-configure-sshguard {
  announce-task "Installing and configuring sshguard"

  install-package sshguard

  if [[ -f ${conf_dir}/sshguard.conf ]]; then
    run-cmd mv ${conf_dir}/sshguard.conf /usr/local/etc
  fi

  run-cmd sysrc sshguard_enable="YES"
  run-cmd 'service sshguard start || echo'
}

function configure-sshd {
  announce-task "Configuring sshd"

  if [[ -f ${conf_dir}/sshd_config ]]; then
    run-cmd mv ${conf_dir}/sshd_config /etc/ssh
  fi

  run-cmd 'service sshd restart'
}

function configure-sysrc {
  if [[ -f ${conf_dir}/sysrc.conf ]]; then
    announce-task "Configuring sysrc"
    run-cmd mv ${conf_dir}/sysrc.conf /etc
  fi
}

function configure-clock-sync {
  announce-task "Setting up clock sync"

  if [[ -f ${conf_dir}/160.synctime ]]; then
    run-cmd mv ${conf_dir}/160.synctime /etc/periodic/daily
    run-cmd chmod +x /etc/periodic/daily/160.synctime
  fi

  if [[ -f ${conf_dir}/synctime ]]; then
    run-cmd mv ${conf_dir}/synctime /usr/local/etc/rc.d
    run-cmd chmod +x /usr/local/etc/rc.d/synctime
  fi
}

update-everything
configure-sysrc
configure-sshd
install-and-configure-pf
install-and-configure-sshguard
install-packages tor security/nyx

prep-and-mount-volume
install-mo
configure-and-start-tor

install-packages srm zsh nano tmux htop
create-my-user

configure-clock-sync
shutdown -r now

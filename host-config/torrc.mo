SOCKSPort 0
SOCKSPolicy reject *

Log notice file /tor/log/tor.log

## Uncomment this to start the process in the background... or use
## --runasdaemon 1 on the command line. This is ignored on Windows;
## see the FAQ entry if you want Tor to run as an NT service.
#RunAsDaemon 1

DataDirectory /tor/db

ControlPort 9051
HashedControlPassword {{HASHED_CONTROL_PASSWORD}}

ORPort 9001

## The IP address or full DNS name for incoming connections to your
## relay. Leave commented out and Tor will guess.
#Address noname.example.com

Nickname {{NICKNAME}}

RelayBandwidthRate 15000 KBytes
RelayBandwidthBurst 50000 KBytes

AccountingMax 3 TBytes
AccountingStart month 1 00:00

ContactInfo via ProtonMail: glorp-relays

DirPort 9030
#DirCache 0

## Uncomment to return an arbitrary blob of html on your DirPort. Now you
## can explain what Tor is if anybody wonders why your IP address is
## contacting them. See contrib/tor-exit-notice.html in Tor's source
## distribution for a sample.
#DirPortFrontPage /usr/local/etc/tor/tor-exit-notice.html

MyFamily {{MYFAMILY}}

ExitRelay 0
ExitPolicy reject *:* # no exits allowed

## What's here

After a host is provisioned, this directory is copied into /tmp on the new host. `configure-tor-host.sh` is invoked to perform the remaining setup

- **50-mkb-sudoer** — Allows user `mkb` to `su` to root
- **
- **configure-tor-host.sh** — Does most of the work, including moving these - **other files where they need to be- **
- **
- **ntp.comf** — Going away soon
- **
- **nyxrc** — Config for the `nyx` command line tool which is used to - **monitor Tor- **
- **
- **torrc.mo** — Mustache template which becomes the Tor config
- **
- **zshrc.zsh** — A few basic settings for the user account we create

## Building .torrc

Each relay needs a slightly different config so we use a templating tool called Mo to build `/tor/etc/torrc` out of `torrc.mo`. Look in `configure-tor-host.sh` for the functions called `install-mo` and `build-torrc` to see the particulars.

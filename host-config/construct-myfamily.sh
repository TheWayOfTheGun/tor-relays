#!/usr/bin/env bash

# Constructs the MyFamily entry for torrc
# https://www.torproject.org/docs/tor-manual.html.en#MyFamily

fingerprint_dir=${fingerprint_dir:-'/root/fingerprints'}

fingerprints=''

cat ${fingerprint_dir}/* | sed 's/.* //' | tr '\n' ',' | sed 's/,$//'

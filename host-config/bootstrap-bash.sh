#!/bin/sh

# Without this we'd be pulling the bash executable w/o TLS which
# craetes a pretty straightforward attack vector.

configure-pkg() {
  if grep -q '\bhttps\b' /etc/pkg/FreeBSD.conf; then
    echo "Pkg already set for HTTPS"
  else
    echo "Updating Pkg system to use HTTPS."
    sed -I '.bak' "s/http/https/g" /etc/pkg/FreeBSD.conf
  fi
}

configure-pkg
pkg install -y bash

#!/usr/bin/env bash

curl -s http://169.254.169.254/metadata/v1/user-data | jq -r ".${1}"

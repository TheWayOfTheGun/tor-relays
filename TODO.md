- document aws keys
- test on clean machine
- proper loop for fingerprints
- host-config/README up to date?

- cron update os and packages, notify
  https://trac.torproject.org/projects/tor/wiki/TorRelayGuide/BSDUpdates

- sysctl bits
- log package & os updates
- more readable generation of user-data

- ipv6
- AccountingRule
- detect whether we need to restart
- exhaustive torrc settings

- verify package integrity

- ssh key fingerprint
- ssmtp instead of sendmail?
- more pf config

- https://github.com/CISOfy/Lynis
- https://vez.mrsk.me/freebsd-defaults.txt
- https://hemantthakur.github.io/2015/02/22/FreeBSD-Server-Hardening.html

#!/bin/bash

set -uex

# So ugly...

mkdir -p fingerprints
ip1=`terraform output tor.ip1` && \
nick1=`terraform output tor.nickname1` && \
scp root@${ip1}:/tor/db/fingerprint fingerprints/${nick1}-fingerprint

ip2=`terraform output tor.ip2` && \
nick2=`terraform output tor.nickname2` && \
scp root@${ip2}:/tor/db/fingerprint fingerprints/${nick2}-fingerprint


# terraform state pull | jq '.modules[0]["resources"][].primary.attributes| { ipv4_address, user_data }'

#!/bin/bash

set -uex

relay_count=`terraform output tor.count`

# for relay in {1..$relay_count-1}
for ((n=0;n<$relay_count;n++))
do
  terraform taint digitalocean_droplet.tor.$n
done

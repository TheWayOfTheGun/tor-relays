variable "relay_count" {}
variable "hashed_control_password" {}

variable "regions" {
  description = "Run the droplets in these DO regions"
  type = "list"
  default = ["blr1", "sfo2"]
}

variable "nicknames" {
  description = "Relay nicknames for torrc"
  type = "list"
  default = ["glorpBlr01", "glorpSfo01"]
}

resource "digitalocean_volume" "tor-data" {
  count = "${var.relay_count}"

  region      = "${element(var.regions, count.index)}"
  name        = "tor-data-${count.index}"
  size        = 5
  description = "Configs, logs, and other Tor data."

  lifecycle {
    prevent_destroy = true
  }
}

resource "digitalocean_droplet" "tor" {
  count = "${var.relay_count}"

  image = "freebsd-11-2-x64-zfs"
  name = "tor-${element(var.regions, count.index)}-${count.index}"
  region = "${element(var.regions, count.index)}"
  size = "1gb"
  private_networking = true
  ipv6 = false
  backups = true
  volume_ids = ["${element(digitalocean_volume.tor-data.*.id, count.index)}"]
  user_data = "{ \"relay_nickname\": \"${element(var.nicknames, count.index)}\", \"hashed_control_password\": \"${var.hashed_control_password}\" }"
  ssh_keys = ["${var.ssh_key_fingerprints}"]

  connection {
    user = "root"
    type = "ssh"
    agent = true
    timeout = "2m"
  }

  provisioner "remote-exec" {
    when = "destroy"
    inline = [
      "shutdown -h now"
    ]
  }

  provisioner "local-exec" {
    when = "destroy"
    command = "say -v Moira 'tor ${count.index} destroyed'"
  }
}

resource "null_resource" "tor-config" {
  count = "${var.relay_count}"

  depends_on = ["digitalocean_droplet.tor"]

  triggers {
    key = "${uuid()}"
  }

  connection {
    user = "root"
    type = "ssh"
    host = "${element(digitalocean_droplet.tor.*.ipv4_address, count.index)}"
    agent = true
    timeout = "2m"
  }

  provisioner "file" {
    source      = "host-config"
    destination = "/root/"
  }

  provisioner "file" {
    source      = "fingerprints"
    destination = "/root/"
  }

  provisioner "remote-exec" {
    inline = [
      "sh /root/host-config/bootstrap-bash.sh",
      "bash /root/host-config/configure-tor-host.sh",
    ]
  }

  provisioner "local-exec" {
    command = "say -v Moira 'Tor ${count.index} deployment done'"
  }
}

output "tor.count" {
  value = "${var.relay_count}"
}

output "tor.ips" {
  value = ["${digitalocean_droplet.tor.*.ipv4_address}"]
}

output "tor.ip1" {
  value = "${digitalocean_droplet.tor.0.ipv4_address}"
}

output "tor.ip2" {
  value = "${digitalocean_droplet.tor.1.ipv4_address}"
}

output "tor.nickname1" {
  value = "${var.nicknames[0]}"
}

output "tor.nickname2" {
  value = "${var.nicknames[1]}"
}

output "tor.nicknames" {
  value = ["${var.nicknames}"]
}

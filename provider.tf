variable "do_token" {}

variable "ssh_key_fingerprints" {
  type = "list"
}

provider "digitalocean" {
  token = "${var.do_token}"
}

terraform {
  required_version = ">= 0.11"

  backend "s3" {}
}

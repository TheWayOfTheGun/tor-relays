vars_file  := digitalocean.tfvars
plan_file  := tor.tfplan

all: deploy

$(plan_file): .terraform
	terraform get -update
	terraform plan -var-file ${vars_file} -out ${plan_file}

deploy: ${plan_file}
	terraform apply -var-file ${vars_file}

ssh:
	ssh -a mkb@`terraform output tor.ip1`

ssh1:
	ssh -a mkb@`terraform output tor.ip1`

ssh2:
	ssh -a mkb@`terraform output tor.ip2`

get-fingerprints: .PHONY
	./get-fingerprints.sh

update-fingerprints: get-fingerprints deploy

rebuild: taint deploy

taint:
	./taint-instances.sh

clean:
	rm -f ${plan_file}
	rm -fr tmp/*

.terraform:
	terraform init -backend-config=backend.tfvars

.PHONY:


pfctl -t sshguard -T show

https://www.sccs.swarthmore.edu/users/16/mmcconv1/tor-hardening.html

https://trac.torproject.org/projects/tor/wiki/TorRelayGuide/BSDUpdates
https://humdi.net/vnstat/
https://github.com/coldhakca/tor-relay-bootstrap/blob/master/bootstrap.sh


# sysrc vars to investigate
    net.inet.ip.process_options=0    # Enable if you need IGMP or multicast.
    net.inet.ip.random_id=1
    net.inet.ip.redirect=0
    net.inet.tcp.cc.algorithm=cubic # Requires that you run "kldload cc_cubic" and
                                      add "cc_cubic_load=YES" to /boot/loader.conf
    net.inet.icmp.drop_redirect=1
    net.inet.tcp.drop_synfin=1
    net.inet.sctp.blackhole=2
    net.inet.tcp.blackhole=2
    net.inet.udp.blackhole=1         # Note the blackhole options can sometimes
                                     # make debugging network issues more difficult.
    net.inet.tcp.icmp_may_rst=0
    security.bsd.hardlink_check_gid=1   # These two options will break poudriere's
    security.bsd.hardlink_check_uid=1   # compiling privsep.
    security.bsd.see_other_gids=0
    security.bsd.see_other_uids=0
    security.bsd.stack_guard_page=1
    security.bsd.unprivileged_proc_debug=0
    security.bsd.unprivileged_read_msgbuf=0
